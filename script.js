const chooseLevel = document.querySelector("#chooseLevel");
const selectLevel = document.querySelector(".selectLevel");
const gameContainer = document.querySelector("#game");
const startGameButton = document.querySelector("#startButton");
const backgroundImage = document.querySelector("#backgroundImage1");
const startGameDiv = document.querySelector("#startGameDiv");
const gameDiv = document.querySelector("#gameDiv");
const messageDiv = document.querySelector("#messageDiv");
const heading = document.querySelector("#heading");
const scores = document.querySelector("#scores");
const records = document.querySelector("#records");
const yourScore = document.querySelector("#yourScore");
const maxScore = document.querySelector("#max");
const attempts = document.querySelector("#attempts");
const restart = document.querySelector("#restart");
const exitButton = document.querySelector("#exit");
const timeCount = document.querySelector("#timeCount");

const flowerImage = "./myGif/flower.jpg";

let scoreGot = 0;
let noOfAttempts = 0;
let firstClickedCard;
let secondClickedCard;
let flipCounts = 0;
let toScore = 12;
let pairSelected = false;
let flagToStopTimer = false;

const gif1 = "./gifs/1.gif";
const gif2 = "./gifs/2.gif";
const gif3 = "./gifs/3.gif";
const gif4 = "./gifs/4.gif";
const gif5 = "./gifs/5.gif";
const gif6 = "./gifs/6.gif";
const gif7 = "./gifs/7.gif";
const gif8 = "./gifs/8.gif";
const gif9 = "./gifs/9.gif";
const gif10 = "./gifs/10.gif";
const gif11 = "./gifs/11.gif";
const gif12 = "./gifs/12.gif";

const gifs = [
  gif1,
  gif2,
  gif3,
  gif4,
  gif5,
  gif6,
  gif7,
  gif8,
  gif9,
  gif10,
  gif11,
  gif12,
];

//Shuffling the Divs
function shuffle(array) {
  array = array.concat([...array]);
  console.log(array);
  let counter = array.length;
  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);
    counter--;
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }
  return array;
}

let shuffledGifs = shuffle(gifs);

//Background Conversion
function backgroundConversion(divElement, image, degree) {
  divElement.style.background = "url(" + image + ")";
  divElement.style.transition = "transform 0.5s";
  divElement.style.transform = "rotateY( " + degree + "deg )";
  divElement.style.backgroundSize = "100% 100%";
  divElement.style.backgroundRepeat = "no-repeat";
}

//Creating Divs for Each GIF
function createDivsForGifs(gifArray) {
  let allGifDivs = gifArray.map((gif) => {
    const newDiv = document.createElement("div");
    newDiv.classList.add(gif);
    backgroundConversion(newDiv, flowerImage, 0);
    newDiv.addEventListener("click", handleCardClick);
    return newDiv;
  });
  console.log(allGifDivs);
  gameContainer.append(...allGifDivs);
  let maxScoreGot = Number(localStorage.getItem("max" + toScore));
  maxScore.innerText = maxScoreGot;
}

//Handling Click Event for Each Button
function handleCardClick(event) {
  if (event.target.style.transform === "rotateY(180deg)") {
    return;
  } else {
    backgroundConversion(event.target, event.target.getAttribute("class"), 180);
    flipCounts++;
  }
  console.log(flipCounts);
  if (flipCounts === 1) {
    firstClickedCard = event.target;
  } else if (flipCounts === 2) {
    secondClickedCard = event.target;
  } else {
    console.log("More than two Selected");
    backgroundConversion(event.target, flowerImage, -180);
    event.target.removeEventListener("click", handleCardClick);
    setTimeout(() => {
      console.log("1 second Complete");
      event.target.addEventListener("click", handleCardClick);
    }, 1 * 1000);
    flipCounts = 0;
    return;
  }

  if (flipCounts === 2) {
    pairSelected = true;
    noOfAttempts++;
    attempts.innerText = noOfAttempts;
  } else {
    pairSelected = false;
  }

  if (pairSelected === true) {
    if (
      firstClickedCard.getAttribute("class") ===
      secondClickedCard.getAttribute("class")
    ) {
      console.log("Matched");
      scoreGot++;
      yourScore.innerText = scoreGot;
      firstClickedCard.removeEventListener("click", handleCardClick);
      secondClickedCard.removeEventListener("click", handleCardClick);
      flipCounts = 0;
      pairSelected = false;
      if (scoreGot === toScore) {
        let finalScore = noOfAttempts;
        let maxScoreGot = localStorage.getItem("max" + toScore);
        if (maxScoreGot === null) {
          maxScoreGot = Infinity;
        }
        maxScoreGot = maxScoreGot < finalScore ? maxScoreGot : finalScore;
        localStorage.setItem("max" + toScore, maxScoreGot);
        maxScore.innerText = maxScoreGot;
        setTimeout(() => {
          gameDiv.style.display = "none";
          messageDiv.style.display = "block";
          restart.style.display = "none";
          flagToStopTimer = true;
          backgroundImage.setAttribute("src", "./myImage/Infinite3.gif");
        }, 1 * 1000);
      }
    } else {
      console.log("Not Matched");
      setTimeout(() => {
        backgroundConversion(firstClickedCard, flowerImage, -180);
        backgroundConversion(secondClickedCard, flowerImage, -180);
        flipCounts = 0;
      }, 1 * 1000);
    }
  } else {
    console.log("Not Yet");
  }
}

function timeCountFunc(min, timeStarts) {
  if (flagToStopTimer) {
    flagToStopTimer = false;
  } else {
    if (timeStarts >= 60) {
      min++;
      timeStarts = 0;
    }
    setTimeout(() => {
      // console.log(timeStarts);
      timeCount.innerText = `${min} min ${timeStarts} sec`;
      timeCountFunc(min, timeStarts + 1);
    }, 1 * 1000);
  }
}

function allEventListeners() {
  //Start The Game From Here
  startGameButton.addEventListener("click", (event) => {
    event.preventDefault();
    if (chooseLevel.value === "hard") {
      toScore = 12;
      backgroundImage.setAttribute("src", "./myImage/Infinite5.gif");
      createDivsForGifs(shuffle(gifs.slice(0, 12)));
    } else if (chooseLevel.value === "intermediate") {
      toScore = 8;
      backgroundImage.setAttribute("src", "./myImage/Infinite1.gif");
      createDivsForGifs(shuffle(gifs.slice(0, 8)));
    } else {
      toScore = 4;
      backgroundImage.setAttribute("src", "./myImage/Infinite2.gif");
      createDivsForGifs(shuffle(gifs.slice(0, 4)));
    }
    startGameDiv.style.display = "none";
    gameDiv.style.display = "block";
    scores.style.display = "block";
    restart.style.display = "block";
    exitButton.style.display = "block";
    records.style.display = "block";
    selectLevel.style.display = "none";
    timeCountFunc(0, 0);
  });

  //Restart the Game From here
  restart.addEventListener("click", (event) => {
    event.preventDefault();
    document.getElementById("game").innerHTML = "";
    scoreGot = 0;
    yourScore.innerText = scoreGot;
    noOfAttempts = 0;
    attempts.innerText = noOfAttempts;
    flipCounts = 0;
    if (chooseLevel.value === "easy") {
      toScore = 4;
      createDivsForGifs(shuffle(gifs.slice(0, 4)));
    } else if (chooseLevel.value === "intermediate") {
      toScore = 8;
      createDivsForGifs(shuffle(gifs.slice(0, 8)));
    } else {
      toScore = 12;
      createDivsForGifs(shuffle(gifs.slice(0, 12)));
    }
    flagToStopTimer = true;
    setTimeout(() => {
      timeCountFunc(0, 0);
    }, 1 * 1000);
  });

  //Exit Button
  exitButton.addEventListener("click", (event) => {
    event.preventDefault();
    location.href = "./index.html";
  });
}

allEventListeners();
